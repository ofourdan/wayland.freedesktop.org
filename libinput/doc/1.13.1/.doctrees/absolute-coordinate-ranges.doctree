��$S      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _absolute_coordinate_ranges:�h]�h}�(h]�h]�h]�h]�h]��refid��absolute-coordinate-ranges�uhhhKhhhhh�F/home/whot/code/libinput/build/doc/user/absolute-coordinate-ranges.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�#Coordinate ranges for absolute axes�h]�h �Text����#Coordinate ranges for absolute axes�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX�  libinput requires that all touchpads provide a correct axis range and
resolution. These are used to enable or disable certain features or adapt
the interaction with the touchpad. For example, the software button area is
narrower on small touchpads to avoid reducing the interactive surface too
much. Likewise, palm detection works differently on small touchpads as palm
interference is less likely to happen.�h]�h9X�  libinput requires that all touchpads provide a correct axis range and
resolution. These are used to enable or disable certain features or adapt
the interaction with the touchpad. For example, the software button area is
narrower on small touchpads to avoid reducing the interactive surface too
much. Likewise, palm detection works differently on small touchpads as palm
interference is less likely to happen.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h��Touchpads with incorrect axis ranges generate error messages
in the form:
<blockquote>
Axis 0x35 value 4000 is outside expected range [0, 3000]
</blockquote>�h]�h9��Touchpads with incorrect axis ranges generate error messages
in the form:
<blockquote>
Axis 0x35 value 4000 is outside expected range [0, 3000]
</blockquote>�����}�(hhVhhThhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hXF  This error message indicates that the ABS_MT_POSITION_X axis (i.e. the x
axis) generated an event outside the expected range of 0-3000. In this case
the value was 4000.
This discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends can be the source of a number of perceived
bugs in libinput.�h]�h9XF  This error message indicates that the ABS_MT_POSITION_X axis (i.e. the x
axis) generated an event outside the expected range of 0-3000. In this case
the value was 4000.
This discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends can be the source of a number of perceived
bugs in libinput.�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�#.. _absolute_coordinate_ranges_fix:�h]�h}�(h]�h]�h]�h]�h]�h*�absolute-coordinate-ranges-fix�uhhhK hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�$Measuring and fixing touchpad ranges�h]�h9�$Measuring and fixing touchpad ranges�����}�(hh�hh~hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh{hhhh,hKubhE)��}�(h� To fix the touchpad you need to:�h]�h9� To fix the touchpad you need to:�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK!hh{hhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h�0measure the physical size of your touchpad in mm�h]�hE)��}�(hh�h]�h9�0measure the physical size of your touchpad in mm�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK#hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�run touchpad-edge-detector�h]�hE)��}�(hh�h]�h9�run touchpad-edge-detector�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK$hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�.trim the udev match rule to something sensible�h]�hE)��}�(hh�h]�h9�.trim the udev match rule to something sensible�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK%hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�Preplace the resolution with the calculated resolution based on physical settings�h]�hE)��}�(hh�h]�h9�Preplace the resolution with the calculated resolution based on physical settings�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK&hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�test locally�h]�hE)��}�(hh�h]�h9�test locally�����}�(hh�hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK'hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�$send a patch to the systemd project
�h]�hE)��}�(h�#send a patch to the systemd project�h]�h9�#send a patch to the systemd project�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK(hj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix��.�uhh�hh{hhhh,hK#ubhE)��}�(h� Detailed explanations are below.�h]�h9� Detailed explanations are below.�����}�(hj9  hj7  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK*hh{hhubhE)��}�(hX~  `libevdev <http://freedesktop.org/wiki/Software/libevdev/>`_ provides a tool
called **touchpad-edge-detector** that allows measuring the touchpad's input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�h]�(h �	reference���)��}�(h�<`libevdev <http://freedesktop.org/wiki/Software/libevdev/>`_�h]�h9�libevdev�����}�(hhhjK  ubah}�(h]�h]�h]�h]�h]��name��libevdev��refuri��.http://freedesktop.org/wiki/Software/libevdev/�uhjI  hjE  ubh)��}�(h�1 <http://freedesktop.org/wiki/Software/libevdev/>�h]�h}�(h]��libevdev�ah]�h]��libevdev�ah]�h]��refuri�j\  uhh�
referenced�KhjE  ubh9� provides a tool
called �����}�(h� provides a tool
called �hjE  hhhNhNubh �strong���)��}�(h�**touchpad-edge-detector**�h]�h9�touchpad-edge-detector�����}�(hhhjr  ubah}�(h]�h]�h]�h]�h]�uhjp  hjE  ubh9X   that allows measuring the touchpad’s input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�����}�(hX   that allows measuring the touchpad's input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�hjE  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK,hh{hhubh �literal_block���)��}�(hX/  $> sudo touchpad-edge-detector /dev/input/event4
Touchpad SynPS/2 Synaptics TouchPad on /dev/input/event4
Move one finger around the touchpad to detect the actual edges
Kernel says:       x [1024..3112], y [2024..4832]
Touchpad sends:    x [2445..4252], y [3464..4071]

Touchpad size as listed by the kernel: 49x66mm
Calculate resolution as:
   x axis: 2088/<width in mm>
   y axis: 2808/<height in mm>

Suggested udev rule:
# <Laptop model description goes here>
evdev:name:SynPS/2 Synaptics TouchPad:dmi:bvnLENOVO:bvrGJET72WW(2.22):bd02/21/2014:svnLENOVO:pn20ARS25701:pvrThinkPadT440s:rvnLENOVO:rn20ARS25701:rvrSDK0E50512STD:cvnLENOVO:ct10:cvrNotAvailable:*
 EVDEV_ABS_00=2445:4252:<x resolution>
 EVDEV_ABS_01=3464:4071:<y resolution>
 EVDEV_ABS_35=2445:4252:<x resolution>
 EVDEV_ABS_36=3464:4071:<y resolution>�h]�h9X/  $> sudo touchpad-edge-detector /dev/input/event4
Touchpad SynPS/2 Synaptics TouchPad on /dev/input/event4
Move one finger around the touchpad to detect the actual edges
Kernel says:       x [1024..3112], y [2024..4832]
Touchpad sends:    x [2445..4252], y [3464..4071]

Touchpad size as listed by the kernel: 49x66mm
Calculate resolution as:
   x axis: 2088/<width in mm>
   y axis: 2808/<height in mm>

Suggested udev rule:
# <Laptop model description goes here>
evdev:name:SynPS/2 Synaptics TouchPad:dmi:bvnLENOVO:bvrGJET72WW(2.22):bd02/21/2014:svnLENOVO:pn20ARS25701:pvrThinkPadT440s:rvnLENOVO:rn20ARS25701:rvrSDK0E50512STD:cvnLENOVO:ct10:cvrNotAvailable:*
 EVDEV_ABS_00=2445:4252:<x resolution>
 EVDEV_ABS_01=3464:4071:<y resolution>
 EVDEV_ABS_35=2445:4252:<x resolution>
 EVDEV_ABS_36=3464:4071:<y resolution>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj�  hK;hh{hhhh,ubhE)��}�(hX|  Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the `systemd project <https://github.com/systemd/systemd>`_.
An example commit can be found
`here <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>`_.�h]�(h9��Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the �����}�(h��Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the �hj�  hhhNhNubjJ  )��}�(h�7`systemd project <https://github.com/systemd/systemd>`_�h]�h9�systemd project�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��systemd project�j[  �"https://github.com/systemd/systemd�uhjI  hj�  ubh)��}�(h�% <https://github.com/systemd/systemd>�h]�h}�(h]��systemd-project�ah]�h]��systemd project�ah]�h]��refuri�j�  uhhjj  Khj�  ubh9�!.
An example commit can be found
�����}�(h�!.
An example commit can be found
�hj�  hhhNhNubjJ  )��}�(h�\`here <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>`_�h]�h9�here�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��here�j[  �Rhttps://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045�uhjI  hj�  ubh)��}�(h�U <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>�h]�h}�(h]��here�ah]�h]��here�ah]�h]��refuri�j�  uhhjj  Khj�  ubh9�.�����}�(hj6  hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKKhh{hhubhE)��}�(h��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:
::�h]�h9��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:�����}�(h��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:�hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKRhh{hhubj�  )��}�(h�Gevdev:name:SynPS/2 Synaptics TouchPad:dmi:*svnLENOVO:*pvrThinkPadT440s*�h]�h9�Gevdev:name:SynPS/2 Synaptics TouchPad:dmi:*svnLENOVO:*pvrThinkPadT440s*�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK\hh{hhhh,ubh �note���)��}�(h��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�h]�hE)��}�(h��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�h]�h9��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKZhj  ubah}�(h]�h]�h]�h]�h]�uhj  hh{hhhh,hNubhE)��}�(h�*The actual axis overrides are in the form:�h]�h9�*The actual axis overrides are in the form:�����}�(hj*  hj(  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK]hh{hhubj�  )��}�(h�;# axis number=min:max:resolution
 EVDEV_ABS_00=2445:4252:42�h]�h9�;# axis number=min:max:resolution
 EVDEV_ABS_00=2445:4252:42�����}�(hhhj6  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hKfhh{hhhh,ubhE)��}�(h�7or, if the range is correct but the resolution is wrong�h]�h9�7or, if the range is correct but the resolution is wrong�����}�(hjF  hjD  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKdhh{hhubj�  )��}�(h�-# axis number=::resolution
 EVDEV_ABS_00=::42�h]�h9�-# axis number=::resolution
 EVDEV_ABS_00=::42�����}�(hhhjR  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hKmhh{hhhh,ubhE)��}�(h��Note the leading single space. The axis numbers are in hex and can be found
in *linux/input-event-codes.h*. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�h]�(h9�ONote the leading single space. The axis numbers are in hex and can be found
in �����}�(h�ONote the leading single space. The axis numbers are in hex and can be found
in �hj`  hhhNhNubh �emphasis���)��}�(h�*linux/input-event-codes.h*�h]�h9�linux/input-event-codes.h�����}�(hhhjk  ubah}�(h]�h]�h]�h]�h]�uhji  hj`  ubh9�S. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�����}�(h�S. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�hj`  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKlhh{hhubj  )��}�(h��The touchpad's ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�h]�hE)��}�(h��The touchpad's ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�h]�h9��The touchpad’s ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKphj�  ubah}�(h]�h]�h]�h]�h]�uhj  hh{hhhh,hNubhE)��}�(hXY  Once a match and override rule has been found, follow the instructions at
the top of the
`60-evdev.hwdb <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>`_
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�h]�(h9�YOnce a match and override rule has been found, follow the instructions at
the top of the
�����}�(h�YOnce a match and override rule has been found, follow the instructions at
the top of the
�hj�  hhhNhNubjJ  )��}�(h�T`60-evdev.hwdb <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>`_�h]�h9�60-evdev.hwdb�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��60-evdev.hwdb�j[  �Ahttps://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb�uhjI  hj�  ubh)��}�(h�D <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>�h]�h}�(h]��
evdev-hwdb�ah]�h]��60-evdev.hwdb�ah]�h]��refuri�j�  uhhjj  Khj�  ubh9��
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�����}�(h��
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKuhh{hhubj�  )��}�(h�$udevadm info /sys/class/input/event4�h]�h9�$udevadm info /sys/class/input/event4�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hh{hhhh,ubhE)��}�(h�tAdjust the command for the event node of your touchpad.
A udev builtin will apply the new axis ranges automatically.�h]�h9�tAdjust the command for the event node of your touchpad.
A udev builtin will apply the new axis ranges automatically.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hh{hhubhE)��}�(h��When the axis override is confirmed to work, please submit it as a pull
request to the `systemd project <https://github.com/systemd/systemd>`_.�h]�(h9�WWhen the axis override is confirmed to work, please submit it as a pull
request to the �����}�(h�WWhen the axis override is confirmed to work, please submit it as a pull
request to the �hj�  hhhNhNubjJ  )��}�(h�7`systemd project <https://github.com/systemd/systemd>`_�h]�h9�systemd project�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��systemd project�j[  �"https://github.com/systemd/systemd�uhjI  hj�  ubh)��}�(h�% <https://github.com/systemd/systemd>�h]�h}�(h]��id1�ah]�h]�h]��systemd project�ah]��refuri�j  uhhjj  Khj�  ubh9�.�����}�(hj6  hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hh{hhubeh}�(h]�(�$measuring-and-fixing-touchpad-ranges�hzeh]�h]�(�$measuring and fixing touchpad ranges��absolute_coordinate_ranges_fix�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j!  hps�expect_referenced_by_id�}�hzhpsubeh}�(h]�(�#coordinate-ranges-for-absolute-axes�h+eh]�h]�(�#coordinate ranges for absolute axes��absolute_coordinate_ranges�eh]�h]�uhh-hhhhhh,hKj$  }�j.  h sj&  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jV  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`424e27f`�h]�jJ  )��}�(h�git commit 424e27f�h]�h9�git commit 424e27f�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/424e27f�uhjI  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f5e21ad5f28>`

�h]�jJ  )��}�(h�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�h]�h9�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f5e21ad5f28>�uhjI  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ahz]�hpau�nameids�}�(j.  h+j-  j*  j!  hzj   j  jf  jc  j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j.  �j-  Nj!  �j   Njf  �j�  �j�  �j�  �uh}�(h+h/j*  h/hzh{j  h{jc  j]  j�  j�  j�  j�  j�  j�  j
  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�2Duplicate explicit target name: "systemd project".�h]�h9�6Duplicate explicit target name: “systemd project”.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]�j
  a�level�K�type��INFO��source�h,�line�Kuhj�  hh{hhhh,hK�uba�transform_messages�]�(j�  )��}�(hhh]�hE)��}�(hhh]�h9�@Hyperlink target "absolute-coordinate-ranges" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�DHyperlink target "absolute-coordinate-ranges-fix" is not referenced.�����}�(hhhj)  ubah}�(h]�h]�h]�h]�h]�uhhDhj&  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�K uhj�  ube�transformer�N�
decoration�Nhhub.